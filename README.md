![graph](/consensus-analyzer/results/graph.png)

по графику видим, что одобрение транзакции будет происходить всегда при количестве сообщений > ~5.4.
наиболее выгодно выбрать самую левую модель с вероятностью подтверждения == 1.0 (в данном случае это модель 2132645391)